package com.celfocus.bigdatabootcamp

object MathFunctions {
  def isPrime(n: Int): Boolean = (2 until n) forall (n % _ != 0)

  def factorial(n: Int): BigInt = if (n == 0) 1 else factorial(n-1) * n
}
