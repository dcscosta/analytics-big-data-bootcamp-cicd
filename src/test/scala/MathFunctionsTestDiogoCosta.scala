package com.celfocus.bigdatabootcamp

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class MathFunctionsTestDiogoCosta extends FunSuite {

  val flPrintLn: Boolean = false

  test("Test: isPrime returns true when number is Prime") {
    // Arrange - Setup of the Scenario
    val inputs = List(1,2,3,5,7,11,13)

    // Act - Executing the action/method
    val outputs = inputs.map(input => MathFunctions.isPrime(input))

    // Assert - Validation of the outcome
    val expectedOuput: Boolean = true
    outputs.map(output => assert(output == expectedOuput))
  }

  test("Test: isPrime returns false when number is not Prime") {
    // Arrange - Setup of the Scenario
    val inputs = List(4,6,8,9,10,12)

    // Act - Executing the action/method
    val outputs = inputs.map(input => MathFunctions.isPrime(input))

    // Assert - Validation of the outcome
    val expectedOuput: Boolean = false
    outputs.map(output => assert(output == expectedOuput))
  }
}