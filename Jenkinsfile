pipeline {
    agent {
        dockerfile {
            args '-v /root/.m2:/root/.m2'
        }
    }

    options {
        disableConcurrentBuilds()
    }

    triggers {
        pollSCM('H/2 * * * *')
    }

    environment {
        GIT_REPOSITORY = "bitbucket.org/dcscosta/analytics-big-data-bootcamp-cicd.git"
        GIT_CREDENTIALS_ID = 'jenkins-ci'
        GIT_EMAIL_CONFIG = 'jenkins.boot@celfocus.com'
        GIT_USERNAME_CONFIG = 'jenkinsBoot'

        SLACK_CHANNEL = '#p-noc-cias'
        SLACK_TEAM_DOMAIN = 'devopscelfocus'
        SLACK_TOKEN = credentials('jenkins-ci-slack-vfgb')

        GIT_TAG_ON_COMMIT_TO_TAG_RELEASE = sh(script: 'git tag -l --points-at ${GIT_COMMIT}', returnStdout: true).trim()
        GIT_COMMIT_TO_TAG_RELEASE = '${GIT_COMMIT}'
        GIT_LAST_TAG_COMMIT = sh(script: 'git describe --abbrev=0 --tags --always', returnStdout: true).trim()

        SONAR_ADDRESS = 'http://10.254.3.149:9000'
    }

    stages {
        stage('Master | Build & Unit Tests') {
            when {
                branch "master"
            }
            steps {
                configFileProvider([configFile(fileId: 'maven-settings-vfgb', variable: 'MAVEN_SETTINGS')]) {
                    sh "mvn clean -s $MAVEN_SETTINGS test"
                }
            }
        }

        stage('Master | SonarQube Analysis') {
            when {
                branch "master"
            }
            steps {
                configFileProvider(
                        [configFile(fileId: 'maven-settings-vfgb', variable: 'MAVEN_SETTINGS')]) {
                    sh "mvn sonar:sonar -Dsonar.log.level='DEBUG' -Dsonar.host.url=${env.SONAR_ADDRESS}"
                }
                junit 'target/surefire-reports/**/*.xml'
                // requires JACOCO plugin
                step([$class: 'JacocoPublisher', execPattern: 'target/jacoco.exec'])
            }
        }

        stage('Master | Deploy SNAPSHOT Jar') {
            when {
                branch "master"
            }
            steps {
                configFileProvider(
                        [configFile(fileId: 'maven-settings-vfgb', variable: 'MAVEN_SETTINGS')]) {
                    sh "mvn -s $MAVEN_SETTINGS deploy -Dnexus.address=${env.NEXUS_ADDRESS} -DskipTests"
                }
            }
        }

        stage('Release | Deploy new Version Jar') {
            when {
                expression { BRANCH_NAME == "release" && env.GIT_TAG_ON_COMMIT_TO_TAG_RELEASE == null }
            }
            steps {
                script {
                    newVersion = getNewReleaseVersion()
                }

                echo "Old version is: ${env.GIT_LAST_TAG_COMMIT}"
                echo "New version is: ${newVersion}"

                sh "git config --global user.email '${env.GIT_EMAIL_CONFIG}'"
                sh "git config --global user.name '${env.GIT_USERNAME_CONFIG}'"
                sh "git reset --hard origin/master"
                sh "git checkout ${env.BRANCH_NAME}"
                sh "git reset --hard origin/${env.BRANCH_NAME}"
                sh "git tag ${newVersion} ${GIT_COMMIT}"

                withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: env.GIT_CREDENTIALS_ID, usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD']]) {
                   sh "git push https://${GIT_USERNAME}:${GIT_PASSWORD}@${env.GIT_REPOSITORY} ${newVersion} "

                   // Create new hotfix branch
                   sh "git checkout -b hotfix/${newVersion} ${newVersion} "
                   sh "git push https://${GIT_USERNAME}:${GIT_PASSWORD}@${env.GIT_REPOSITORY} hotfix/${newVersion} "
                }

                // requires Managed Files with maven-settings entry
                configFileProvider(
                [configFile(fileId: 'maven-settings-vfgb', variable: 'MAVEN_SETTINGS')]) {
                sh "mvn -s $MAVEN_SETTINGS versions:set -DnewVersion=${newVersion}"
                sh "mvn -s $MAVEN_SETTINGS deploy -Dnexus.address=${env.NEXUS_ADDRESS} -DskipTests"
                }
            }
        }
    }

    post {
        changed {
            notifySlack()
        }
    }
}

/*****************************************
 * To use this function you need to install
 * the Slack Notification Plugin
 ****************************************/
def notifySlack(additionalInfo = '') {
    def colorCode = '#79ae40'
    def status = 'SUCCESS'
    if (currentBuild.result == 'FAILURE') {
        colorCode = '#d34e56'
        status = 'FAILURE'
    }
    def commitText = sh(returnStdout: true, script: 'git show -s --format=format:"*%s*  _by %an_" HEAD').trim()
    def subject = "${env.JOB_NAME} - #${env.BUILD_NUMBER} ${status} (<${env.BUILD_URL}|Open>)"
    def summary = "${subject}\nChanges: ${commitText}\nBranch: ${env.GIT_BRANCH}\n${additionalInfo}"
    slackSend channel: "${env.SLACK_CHANNEL}", color: colorCode, message: summary, teamDomain: "${env.SLACK_TEAM_DOMAIN}", token: "${env.SLACK_TOKEN}"
}

def getNewReleaseVersion() {
    version = env.GIT_LAST_TAG_COMMIT

    def (major,minor,patch) = version.tokenize('.')

    // Increment patch only
    int newMinor = minor.toInteger() + 1

    newVersion = major + "." + newMinor.toString() + "." + patch
    echo newVersion
    newVersion
}